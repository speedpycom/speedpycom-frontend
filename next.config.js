/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,

  images: {
    disableStaticImages: true,
  },
  eslint: {
    dirs: ['pages', 'components', 'theme', 'utils', 'contexts', 'store'],
  },
}

module.exports = nextConfig
