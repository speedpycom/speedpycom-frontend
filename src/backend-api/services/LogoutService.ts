/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Logout } from '../models/Logout';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class LogoutService {

    /**
     * @returns Logout
     * @throws ApiError
     */
    public static logoutCreate(): CancelablePromise<Logout> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/logout/',
        });
    }

}
