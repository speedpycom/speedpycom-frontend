/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TokenObtainPairRequest = {
    email: string;
    password: string;
};

