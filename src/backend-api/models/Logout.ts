/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Logout = {
    readonly status: string;
};

