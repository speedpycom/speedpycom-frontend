/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type User = {
    readonly id: string;
    email: string;
    first_name?: string;
    last_name?: string;
    is_email_confirmed?: boolean;
};

