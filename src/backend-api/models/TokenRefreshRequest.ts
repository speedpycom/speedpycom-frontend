/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TokenRefreshRequest = {
    refresh: string;
};

