/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TokenObtainPair = {
    readonly access: string;
    readonly refresh: string;
};

