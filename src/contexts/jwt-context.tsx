import {createContext, useEffect} from 'react';
import type {FC, ReactNode} from 'react';
import PropTypes from 'prop-types';

import {
  TokenService,
  User
} from "@/backend-api";
import {useRouter} from "next/router";
import useDashboardStore from "../store";
import {UserService} from '@/backend-api/services/UserService';

interface State {
  isInitialized: boolean;
  isAuthenticated: boolean;
  user: User | null;
}

export interface AuthContextValue extends State {
  platform: 'JWT';
  login: (email: string, password: string) => Promise<void>;
  logout: () => Promise<void>;
}

interface AuthProviderProps {
  children: ReactNode;
}

const initialState: State = {
  isAuthenticated: false,
  isInitialized: false,
  user: null
};


export const AuthContext = createContext<AuthContextValue>({
  ...initialState,
  platform: 'JWT',
  login: () => Promise.resolve(),
  logout: () => Promise.resolve()
});

export const AuthProvider: FC<AuthProviderProps> = (props) => {
  const {children} = props;
  const {setUser, unsetUser, initializeFn, user, isAuthenticated, isInitialized} = useDashboardStore(
    state => ({
      setUser: state.setUser,
      unsetUser: state.unsetUser,
      initializeFn: state.initialize,
      user: state.user,
      isAuthenticated: state.isAuthenticated,
      isInitialized: state.isInitialized
    })
  )
  const router = useRouter();
  useEffect(() => {
    const keepRefreshed = async (): Promise<void> => {
      try {
        const accessToken = globalThis.localStorage.getItem('accessToken');
        const refreshToken = globalThis.localStorage.getItem('refreshToken');
        if (accessToken && refreshToken) {
          const {access, refresh} = await TokenService.tokenRefreshCreate(
            {refresh: refreshToken});
          localStorage.setItem('accessToken', access);
          localStorage.setItem('refreshToken', refresh);
        }
      } catch (err) {
        console.error(err);
      }
    };
    const initialize = async (): Promise<void> => {
      try {
        const accessToken = globalThis.localStorage.getItem('accessToken');
        const refreshToken = globalThis.localStorage.getItem('refreshToken');

        if (accessToken && refreshToken) {
          let user: User;
          try {
            user = await UserService.userInfoRetrieve();
          } catch (err) {
            const {access, refresh} = await TokenService.tokenRefreshCreate(
              {refresh: refreshToken});
            localStorage.setItem('accessToken', access);
            localStorage.setItem('refreshToken', refresh);
            user = await UserService.userInfoRetrieve();
          }
          initializeFn({user, isAuthenticated: true});
        } else {
          initializeFn({user: null, isAuthenticated: false});
        }
      } catch (err) {
        console.error(err);
        initializeFn({user: null, isAuthenticated: false});
      }
    };
    initialize();
    const interval = setInterval(keepRefreshed, 1000 * 60 * 2);
    return (): void => {
      clearInterval(interval);
    }
  }, [initializeFn, setUser, unsetUser]);

  const login = async (email: string, password: string): Promise<void> => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    unsetUser();
    const {access, refresh} = await TokenService.tokenCreate(
      {email, password});

      localStorage.setItem('accessToken', access);
      localStorage.setItem('refreshToken', refresh);
      const user = await UserService.userInfoRetrieve();
      setUser(user);

  };

  const logout = async (): Promise<void> => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    unsetUser();
  };


  return (
    <AuthContext.Provider
      value={{
        user, isAuthenticated, isInitialized,
        platform: 'JWT',
        login,
        logout
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const AuthConsumer = AuthContext.Consumer;
