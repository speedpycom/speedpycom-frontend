import {createTheme, responsiveFontSizes} from "@mui/material/styles";
import {PaletteOptions, ThemeOptions} from "@mui/material";
import {createTypography} from "@/theme/typography";

const createOptions = (): ThemeOptions => {
  return {
    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 960,
        lg: 1280,
        xl: 1920,
      }
    },
    shape: {
      borderRadius: 8,
    },
    typography: createTypography(),
    palette:{}
  }
}
const theme = responsiveFontSizes(createTheme(createOptions()));

export default theme;