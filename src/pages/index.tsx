import type {NextPage} from 'next';
import NextLink from 'next/link';
import {Box, Button, Stack, Typography} from "@mui/material";
import {useRouter} from "next/router";
import {Logo} from "@/components/logo";
import Head from 'next/head';

const Page: NextPage = () => {
  const router = useRouter();
  // return a <Box> centered on the screen
  return <>
    <Head>
      <title>
        Django + NextJS SaaS Boilerplate | SpeedPy.com
      </title>
      <meta property="og:type" content="product"/>
      <meta property="og:title" content="Django + NextJS SaaS Boilerplate | SpeedPy.com"/>
      <meta property="og:description"
            content="Django + NextJS SaaS Boilerplate: Super fast way to start Django API backed project with beautiful interface."/>
      <meta property="og:image" content="https://speedpy.com/speedpycom/speedpycom_og_image.png"/>
      <meta property="og:locale" content="en_US"/>
      <meta property="og:site_name" content="SpeedPy.com"/>
    </Head>
    <Box
      sx={{
        alignItems: 'center',
        backgroundColor: 'background.paper',
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        justifyContent: 'center',
        left: 0,
        p: 3,
        position: 'fixed',
        top: 0,
        width: '100vw',
        zIndex: 1400
      }}
    >
      <Box
        sx={{
          display: 'inline-flex',
          height: 310,
          width: 310
        }}
      >
        <Stack spacing={3} sx={{
          alignItems: 'center'
        }}>
          <Logo size={'310'}/>
        </Stack>
      </Box>
      <Typography variant={'h2'}>
        SpeedPy.com
      </Typography>
      <Typography variant={'h4'} sx={{mt: 2}}>
        The only SaaS boilerplate you{"'"}ll ever need.
      </Typography>

      <Typography variant={'h5'} sx={{mt: 2}}>
        Django & DRF + NextJS & MUI = 🚀
      </Typography>

      <Stack direction={'row'} spacing={2} sx={{mt: 3}}>
        <Button component={NextLink} variant={'contained'} size={'large'}
                href={'https://gitlab.com/speedpycom/speedpycom-frontend'}
                target={'_blank'} rel={'noopener'}>Frontend Repo</Button>
        <Button component={NextLink} variant={'contained'} size={'large'}
                href={'https://gitlab.com/speedpycom/speedpycom-backend'}
                target={'_blank'} rel={'noopener'}>Backend Repo</Button>
      </Stack>

      <Stack direction={'row'} spacing={2} sx={{mt: 3}}>
        <Button component={NextLink} variant={'text'} size={'large'}
                href={'https://twitter.com/speedpycom'}
                target={'_blank'} rel={'noopener'}>@speedpycom</Button>
        <Button component={NextLink} variant={'text'} size={'large'}
                href={'https://appliku.com/discord'}
                target={'_blank'} rel={'noopener'}>Join Discord</Button>
      </Stack>
      <Stack direction={'row'} spacing={2} sx={{mt: 3}}>
        <Button component={NextLink} variant={'outlined'} size={'large'}
                href={'https://appliku.com/'}
                target={'_blank'} rel={'noopener'}>Deploy With Appliku</Button>
      </Stack>

    </Box>
  </>;
}

Page.getLayout = (page) => (
  <>
    {page}
  </>
);
export default Page;