import type {FC} from 'react';
import Document, {Html, Head, Main, NextScript} from 'next/document'

const Favicon: FC = () => (
  <>
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/apple-touch-icon.png"
    />
    <link
      rel="icon"
      href="/favicon.ico"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="/favicon-32x32.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="16x16"
      href="/favicon-16x16.png"
    />
  </>
);
const Rewardful: FC = () => {
  return <>
    <script
      dangerouslySetInnerHTML={{
        __html: `(function(w,r){w._rwq=r;w[r]=w[r]||function(){(w[r].q=w[r].q||[]).push(arguments)}})(window,'rewardful');`
      }}
    />
    <script async src='https://r.wdfl.co/rw.js' data-rewardful='f96408'/>
  </>
}

class CustomDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <Rewardful/>
          <Favicon/>
        </Head>
        <body>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    )
  }
}

export default CustomDocument;