import type {AppProps} from 'next/app'
import Head from 'next/head';
import theme from 'src/theme';
import {ThemeProvider} from '@mui/material/styles';
import {Toaster} from "@/components/toaster";
import {CssBaseline} from "@mui/material";


export default function App(props: AppProps) {
  const {Component, pageProps} = props;
  const getLayout = Component.getLayout ?? ((page) => page);
  return (
    <>
      <Head>
        <title>
          Django + NextJS SaaS Boilerplate | SpeedPy.com
        </title>
        {/*meta description*/}
        <meta name="description"
              content="Django + NextJS SaaS Boilerplate: Super fast way to start Django API backed project with beautiful interface."/>

        <meta
          name="viewport"
          content="initial-scale=1, width=device-width"
        />
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        {getLayout(<Component {...pageProps} />)}
        <Toaster/>
      </ThemeProvider>
    </>
  )
}
