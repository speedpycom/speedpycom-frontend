import {styled} from '@mui/material/styles';
import Image from "next/image";

type LogoSize = '32' | '48' | '128' | '180' | '192' | '256' | '310';

interface LogoProps {
  size?: LogoSize;
}

export const Logo = styled((props: LogoProps) => {
  const {size} = props;

  return (
    // eslint-disable-next-line @next/next/no-img-element
    <Image src={`/speedpycom/logo.png`}
         alt={'SpeedPy.com'}
         width={size}
         height={size}
         style={{padding: '0', margin: '0'}}
         decoding={'sync'}
         loading={'eager'}/>
  )
})``;

Logo.defaultProps = {
  size: '48'
};
