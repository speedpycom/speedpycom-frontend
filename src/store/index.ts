import {User} from "@/backend-api";
import { create } from "zustand";
import {devtools} from 'zustand/middleware'

interface InitializePayload {
  user: User | null,
  isAuthenticated: boolean
}

interface dashboardInterface {
  isInitialized: boolean;
  user: User | null;
  isAuthenticated: boolean;
  setUser: (user: User) => void;
  unsetUser: () => void;
  initialize: (payload: InitializePayload) => void;
}

const useDashboardStore = create<dashboardInterface>()(
  devtools((set, get) => (
    {
      isInitialized: false,
      user: null,
      isAuthenticated: false,
      setUser: (user: User) => set({user, isAuthenticated: true}),
      unsetUser: () => set({user: null, isAuthenticated: false}),
      initialize: (payload: InitializePayload) => set({...payload, isInitialized: true})
    }
  )));

export default useDashboardStore;